#!/usr/bin/lua5.3
--[[--
 @package   Mosquitto
 @filename  init.lua
 @version   1.0
 @autor     Diaz Urbaneja Victor Diego Alejandro <sodomon@openmailbox.org>
 @autor     Díaz Devera Víctor Diex Gamar <vitronic2@gmail.com>
 @date      15:40:38 -04 2019
]]

require('lib.middleclass')                  -- la libreria middleclass me da soporte a OOP
local mqtt      = require('mosquitto')      --la libreria que soporta el protocolo
local lgi       = require('lgi')            -- requiero esta libreria que me permitira usar GTK

local GObject   = lgi.GObject               -- parte de lgi
local Gdk       = lgi.Gdk                   -- parte de lgi
local GLib      = lgi.GLib                  -- para el treeview
local Gtk       = lgi.require('Gtk', '3.0') -- el objeto GTK

local assert    = lgi.assert
local builder   = Gtk.Builder()

assert(builder:add_from_file('vistas/chat.ui'),"error al cargar el archivo") -- hago un debugger, si este archivo existe (true) enlaso el archivo ejemplo.ui, si no existe (false) imprimo un error
local ui = builder.objects
local main_window = ui.main_window  -- invoco la ventana con el id main_window


local broker 	  = 'ispcore.com.ve'  --El servidor
local port 		  = 1883              --El puerto
local keepalive	= 60                --Si me desconecto, ¿Cuanto tiempo esperar antes de recoenctar?
local qos 		  = 2                 --Acuse de recibo
--local retain 	= true
local mqtt_user     = 'diego'
local mqtt_password = 'vitronic'

client 			= mqtt.new(mqtt_user..'-lua',false)	--[[ La instancia de comunicacion  MQTT]]--

--client.ON_CONNECT = function()
 --       client:subscribe('users/chat/tontos', 2)
--end

local  dialog_login         = ui.dialog_login                           -- invoco al boton con el id btn_correr
local  about_window         = ui.about_window                           -- invoco al boton con el id btn_correr
local  mensajes             = builder:get_object("mensajes")            -- invoco al boton con el id btn_correr
local  buffer               = builder:get_object("buffer_mensajes")     -- invoco al boton con el id btn_correr
local  btn_enviar           = builder:get_object("btn_enviar")          -- invoco al boton con el id btn_cerrar
local  entry_mensaje        = builder:get_object("entry_mensaje")       -- invoco al boton con el id load_choser
local  btn_aceptar          = builder:get_object("btn_aceptar")         -- invoco al boton con el id load_choser

msg = nil
local mark = buffer:create_mark(nil, buffer:get_end_iter(), false)
GLib.timeout_add(
    GLib.PRIORITY_DEFAULT, 300,
        function()
            if msg then
                buffer:insert(buffer:get_iter_at_mark(mark),
                      '\n'
                       .. os.date('%H:%M:%S ') .. msg, ---esto es el mensaje recibido
                       -1)
                mensajes:scroll_mark_onscreen(mark)
                msg = nil
            end
            return true
        end
)

client.ON_MESSAGE = function(mid, topic, payload)
    if(payload ~= 'my payload') then -- no es obbligado
        msg = payload
    end
end

client:login_set(mqtt_user, mqtt_password)              --Me conecto al MQTT
client:connect(broker,port,keepalive)
client:loop_start()
entry_mensaje:grab_focus()

--cuando se presione Enviar
function enviar()
    mensaje = entry_mensaje.text
    if( mensaje ~= '' ) then                            --solo envio si hay mensaje
        client:publish('users/chat/tontos', '['..mqtt_user..'] ' ..mensaje)    --mando el mensaje
        entry_mensaje.text = '' --limpio el entry
        os.execute("play sound/mensaje.ogg &")
    end
    entry_mensaje:grab_focus() -- @TODO hay que hacer foco en el entry
end

--cuando se presione Enviar
function ui.btn_enviar:on_clicked()
    enviar()
end

--cuando se presione Enviar
function ui.entry_mensaje:on_key_release_event(env)
    if ( env.keyval  == Gdk.KEY_Return ) then
      enviar()
    end
end


--que hacer cuando cierren la ventana principal
function main_window:on_destroy()
    client:disconnect()     --desconecto del mqtt
    Gtk.main_quit()         --Cierro la ventana
end

function status()
    local c = io.popen("ping -q -c 2 ispcore.com.ve | grep transmitted",r)
    local _status = c:read('*a')
    c:close()
    if _status == '2 packets transmitted, 0 packets received, 100% packet loss' or _status == 'ping: sendto: Network unreachable ' then
        ui.status.label = "No hay Conexion"
        return false
    end
    print(_status)
    ui.status.label = "hay Conexion"
    return true
end

status()

function ui.btn_cancelar_preferences:on_clicked()
    ui.preferences_window:hide()
end

function ui.preferences:on_clicked()
	ui.preferences_window:show()
end

--cuando se presione About
function ui.menu_about:on_clicked()
    ui.about_window:run()
    ui.about_window:hide()
end

--cuando se presione Quit
function ui.menu_quit:on_clicked()
   Gtk.main_quit()
end

--function ui.switch_volumen_preferences:on_activate()
--   switch_volumen_preferences.activate true()
--end

main_window:show_all()
Gtk.main()